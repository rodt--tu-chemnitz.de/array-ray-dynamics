LANG=en_us

# parameters
n_limacons=1
deform=0.90
DISTperR=3.0
n_edgepoints=80
n_rays=200



# create folder
target_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
target_folder="data/$target_folder"
mkdir -p "$target_folder"

# save input
input="$n_limacons $deform $DISTperR $n_edgepoints $n_rays $target_folder"

# execute code
python main.py $input # > "${target_folder}/outfile.txt"