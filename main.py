import numpy as np
import random

from sys import argv
import os

import datetime
import time

import modules.plotparams as plotparams

from modules.class_limacon import Limacon
from modules.class_system import System


def main():
    time_start = datetime.datetime.now()

    plotparams.centertitle('LIMACON ARRAY RAY-DYNAMICS CALCULATOR', width=70)

    print('reading input...')

    n_limacons = int(argv[1])
    deform = float(argv[2])
    DISTperR = float(argv[3])
    n_edgepoints = int(float(argv[4]))
    n_rays = int(argv[5])
    target_folder = argv[6]

    R = 1.
    dist = DISTperR * R

    print('\t{0} limacons'.format(n_limacons))
    print('\tdeform = {0}'.format(deform))
    print('\tDISTperR = {0}\t-> R = {1}'.format(DISTperR, R))
    print('\t\t\t-> dist = {0}'.format(dist))
    print('\tn_edgepoints = {0}'.format(n_edgepoints))
    print('\tn_rays = {0}'.format(n_rays))


    print('constructing geometry...')

    # limacon parameters
    print('\t- assigning values')


    limacon_params = dict(
        rot=np.pi * .5,
        R=R,
        deform=deform,
        n_edgepoints=n_edgepoints
        )

    # create limacons
    print('\t- creating entities')

    center_x_coords = np.arange(n_limacons) * dist
    center_x_coords = center_x_coords - np.mean(center_x_coords)
    center_coords = [ np.array([x,0]) for x in center_x_coords ]

    geometry = [Limacon(center=xy, **limacon_params) for xy in center_coords ]


    # create system
    print('\t- creating system')

    sys = System(
        geometry=geometry,
        )

    print()
    sys.plot_structure_aio(
        title='{0}/structure.png'.format(target_folder)
    )
    
    # ONE RAY
    if False:
        print('\t\t-> single ray')

        raydir = 'ray_plots'

        if raydir not in os.listdir('.'):
                os.mkdir(raydir)

        if True:
            # check if ray_plots folder exists
            # if not, creates one

            n_tries = 100
            
            for i in range(n_tries):
                np.random.seed(i)
                print('\tseed = {0}'.format(i))
                sys.plot_ray(title='{0}/ray_seed_{1}.png'.format(raydir, i))
        else:
            seed_singleray = 32
            random.seed(seed_singleray)
            sys.plot_ray(title='{0}/singleray_{1}.png'.format(raydir, seed_singleray))
    
    # return

    print()
    # PHASESPACE
    if True:
        seed = time.time()
        random.seed(seed)
        print('\tseed = {0} -> saving'.format(seed))
        np.savetxt(
            '{0}/seed.dat'.format(target_folder), 
            [seed]
            )

        # sys.calc_phasespace(n_rays=2)

        sys.plot_phasespace(
            n_rays=n_rays,
            outfilename='{0}/phasespace.png'.format(target_folder)
            )

    time_end = datetime.datetime.now()
    
    time_needed = time_end - time_start

    print('\n\nTime needed: {0}'.format(time_needed))
    np.savetxt(
        '{0}/time.dat'.format(target_folder), 
        [time_needed.total_seconds()]
        )
    
    print('DONE!')




if __name__ == '__main__':
    main()