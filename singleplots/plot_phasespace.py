import sys
sys.path.append('..')

from modules.toolbox import plot_phasespace_pixels
import modules.plotparams

import numpy as np

def main():
    # parameters
    n_limacons=1
    deform=0.40
    DISTperR=3.0
    n_edgepoints=14
    n_rays=10000

    datapath = '../data/limacons-{0}/deform-{1:.2f}_dpr-{2:.1f}_edgepoints-{3}_rays-{4}/phasespace.dat'
    data = np.loadtxt(datapath.format(n_limacons, deform, DISTperR, n_edgepoints, n_rays))
    
    plot_phasespace_pixels(
        *data,
        n_objects=n_limacons,
        n_refraction=3.8,
        outfilename='phasespace.png'
        )

if __name__ == '__main__':
    main()