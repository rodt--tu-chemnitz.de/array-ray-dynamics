LANG=en_us

# parameters
n_limacons=3
deform=0.90
#DISTperR=3.0
n_rays=10000
n_edgepoints=80

echo "VARY DIST"

for DISTperR in $(seq 2.55 0.05 3. ); do
    echo $DISTperR

    # create folder
    target_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
    target_folder="data/$target_folder"
    mkdir -p "$target_folder"

    # save input
    input="$n_limacons $deform $DISTperR $n_edgepoints $n_rays $target_folder"

    # execute code
    python main.py $input > "${target_folder}/log.log"
done