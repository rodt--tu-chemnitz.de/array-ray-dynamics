LANG=en_us

# parameters
n_limacons=1
deform=0.40
DISTperR=3.00
n_rays=10000
# n_edgepoints=80

# filenames
ps_filename="phasespace.png"
psp_filename="phasespace_pixels.png"
ff_filename="phasespace_ff.png"
sys_filename="structure.png"

# target folder to compare
target_folder="compare_edgepoints"
rm -r ${target_folder}
mkdir -p ${target_folder}

# create folder

echo "VARY DIST"

for n_edgepoints in $(seq 5 1 100); do
    echo $n_edgepoints

    resource_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
    resource_folder="../data/${resource_folder}"

    cp "${resource_folder}/${ps_filename}" "${target_folder}/ps_${n_edgepoints}.png"
    cp "${resource_folder}/${psp_filename}" "${target_folder}/psp_${n_edgepoints}.png"
    cp "${resource_folder}/${ff_filename}" "${target_folder}/ff_${n_edgepoints}.png"
    cp "${resource_folder}/${sys_filename}" "${target_folder}/sys_${n_edgepoints}.png"

done