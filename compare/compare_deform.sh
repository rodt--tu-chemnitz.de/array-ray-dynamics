LANG=en_us

# parameters
n_limacons=3
# deform=0.40
DISTperR=3.00
n_rays=10000
n_edgepoints=80

# filenames
ps_filename="phasespace.png"
psp_filename="phasespace_pixels.png"
ff_filename="phasespace_ff.png"
sys_filename="structure.png"

# target folder to compare
target_folder="compare_deform"
rm -r ${target_folder}
mkdir -p ${target_folder}

# create folder

echo "VARY DEFORM"

for deform in $(seq 0.20 0.05 0.9 ); do
    echo $deform

    resource_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
    resource_folder="../data/${resource_folder}"

    cp "${resource_folder}/${ps_filename}" "${target_folder}/ps_${deform}.png"
    cp "${resource_folder}/${psp_filename}" "${target_folder}/psp_${deform}.png"
    cp "${resource_folder}/${ff_filename}" "${target_folder}/ff_${deform}.png"
    cp "${resource_folder}/${sys_filename}" "${target_folder}/sys_${deform}.png"
    
    echo
done