LANG=en_us

# parameters
n_limacons=1
deform=0.40
DISTperR=3.00
n_rays=20000

echo "VARY EDGEPOINTS"

for n_edgepoints in $(seq 5 5 100 ); do
    echo $n_edgepoints

    # create folder
    target_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
    target_folder="data/$target_folder"
    mkdir -p "$target_folder"

    # save input
    input="$n_limacons $deform $DISTperR $n_edgepoints $n_rays $target_folder"

    # execute code
    python main.py $input > "${target_folder}/log.log"
done