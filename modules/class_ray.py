import numpy as np
from shapely.geometry import Point

from numpy.linalg import norm
from numpy import pi, sin, cos, arccos, arcsin, dot, cross, abs

from modules.toolbox import rotmat

class Ray():
    def __init__(self, 
        starting_point=np.array([0,0]), starting_direction=np.array([1,0])
        ):

        # history of light ray
        self.points = [starting_point]
        self.vectors = [starting_direction]
        self.s_list = []
        self.phi_list = []

        # present situation
        self.p = starting_point
        self.v = starting_direction


    def print(self):
        print('Vector ', self)

        print('points:')
        for p in self.points:
            print('\t', p)
        
        print('vectors:')
        for v in self.vectors:
            print('\t', v)

    ###########################################################
    # Ray Dynamics
    ###########################################################

    def bounce(self, system):
        '''
        calculates the next reflection, given the system and the last reflection point
        and direction
        '''
        # print('\nbounce')
        
        crossing_points = []

        # p = a * t + b
        a_1 = self.v
        b_1 = self.p
        

        # calculate crossings with geometry
        
        for obj, s_obj in zip(system.geometry, system.length_list):
            # print(obj)
            for i in range(obj.n_edgepoints):
                p_1 = obj.edge[i]
                p_2 = obj.edge[i+1]

                # rename to make it clearer
                a_2 = p_2 - p_1
                b_2 = p_1

                # calculate
                db = b_2 - b_1
                axa = cross(a_1, a_2)

                t_1 = cross( db, a_2) / axa
                t_2 = cross( db, a_1) / axa


                
                # print(t_1, '\t', t_2)

                # found cross
                if 0. < t_1 and 0. < t_2 and t_2 < 1. and 10**(-10) < abs(t_1):
                    # crossing point
                    p_cross = a_1 * t_1 + b_1
                    # arc length
                    s = s_obj + obj.length_list[i] + t_2 * norm(a_2)

                    crossing_points.append([t_1, p_cross, obj, a_2, b_2, s])
        

        # signal that the ray left the system
        if len(crossing_points) == 0:
            return False


        # get closest valid crossing point
        crossing_points = sorted(
            crossing_points,
            key=lambda x: x[0]
            )
        
        # print('crossing points = ', crossing_points)
        
        # get best point
        t_1, p_cross, obj, a_2, b_2, s = crossing_points[0]



        # calculate reflection

        a_2 = a_2 / norm(a_2)

        # incident angle
        # dotprod > 0 -> counter-clockwise
        # dotprod < 0 -> clockwise
        dotprod = dot(a_1, a_2)
        # print('dotprod = ', dotprod)

        theta = arccos(dotprod)
        if dotprod < .0:
            theta = theta - pi

        # print('theta = {0} pi'.format(theta / pi))
        
        # always calculate incident angle for phase space
        phi_1 = pi * .5 - abs(theta)
        # print('phi_1 = {0} pi'.format(phi_1 / pi))


        # check if ray is inside or outside objects
        checkpoint = Point(a_1 * t_1 * .5 + b_1)
        
        # inside -> outside
        # critical angle exists
        if checkpoint.within(obj.polygon):
            # print('inside -> outside')

            # reflecting inside
            if abs(theta) < obj.theta_crit:
                # print('-> total reflection')

                if dotprod > .0:
                    v_new = a_2 @ rotmat(theta)
                else:
                    v_new = -a_2 @ rotmat(theta)

            # reflecting outside
            else:
                # print('-> reflection outside')
                # using snellius' law
                # n_1 * sin(phi_1) = n_2 * sin(phi_2)

                phi_2 = arcsin( obj.n_refraction * sin(phi_1) )
                # print('phi_2 = {0} pi'.format(phi_2 / pi))

                theta_2 = pi * .5 - phi_2
                
                # print('theta_2 = {0} pi'.format(theta_2 / pi))
                
                if dotprod < .0:
                    v_new = - a_2 @ rotmat(theta_2)
                else:
                    v_new = a_2 @ rotmat(-theta_2)


        # outside -> inside
        # ray always transmits using snellius' law
        # n_1 * sin(phi_1) = n_2 * sin(phi_2)
        else:
            # print('outside -> inside')

            phi_2 = arcsin(sin(phi_1) / obj.n_refraction )
            # print('phi_2 = {0} pi'.format(phi_2 / pi))

            theta_2 = pi * .5 - phi_2
            
            # print('theta_2 = {0} pi'.format(theta_2 / pi))

            if dotprod < .0:
                v_new = (-a_2) @ rotmat(-theta_2)
            else:
                v_new = a_2.copy() @ rotmat(theta_2)

            



        # saving
        self.p = p_cross
        self.v = v_new

        self.points.append(self.p)
        self.vectors.append(self.v)

        if theta <.0:
            self.phi_list.append(-phi_1)
        else:
            self.phi_list.append(phi_1)
        self.s_list.append(s)

        return True