import numpy as np
import random

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import modules.toolbox as tb
from modules.toolbox import rotmat
from modules.class_ray import Ray

class System():
    ####################################################
    # Init & Properties
    ####################################################

    def __init__(self, geometry=[]):
        # all geometric objects
        self.geometry = geometry
        self.n_objects = len(self.geometry)

        # total surface area of all objects in the system
        # used to determine startoing point of rays
        self.length_total = 0.
        self.length_list = [0.]
        for g in self.geometry:
            self.length_total += g.length_total
            self.length_list.append(self.length_total)
        
        self.length_list = np.array(self.length_list)
        # print(self.length_list)


        # obtain bounding box

        bounds = []

        for g in self.geometry:
            bounds.append(g.bounds)

        # get bounds
        minx_list, miny_list, maxx_list, maxy_list = np.transpose(bounds)

        minx = np.min(minx_list)
        miny = np.min(miny_list)
        maxx = np.max(maxx_list)
        maxy = np.max(maxy_list)


        self.border_factor = .05

        delta_x = maxx - minx
        border_x = delta_x * self.border_factor
        delta_y = maxy - miny
        border_y = delta_y * self.border_factor
        border = np.max([border_x, border_y])

        # define bouning box
        self.bounding_box = np.array([
            [minx - border, maxx + border],
            [miny - border, maxy + border]
            ])


    ####################################################
    # Ray Dynamics
    ####################################################

    def random_wg_vector(self, theta_max=0.2*np.pi):
        # generate whisper gallery starting vector
        s_start = random.random() * self.length_total
    

        # check which object it is from
        for i in range(self.n_objects):
            if self.length_list[i] < s_start and s_start < self.length_list[i+1]:
                s_remaining = s_start - self.length_list[i]
                i_object = i
                break
        else:
            print('ERROR: total surface length doesnt match list')
            print('total_surface = {0}'.format(self.length_total))
            print('surface list = {0}'.format(self.length_list))
            raise(IndexError)
        

        # check where on object point is
        origin_object = self.geometry[i_object]

        for i in range(origin_object.n_edgepoints):
            l_1 = origin_object.length_list[i]
            l_2 = origin_object.length_list[i+1]

            if l_1 < s_remaining and s_remaining < l_2:
                i_pointpos = i
                s_remaining = s_remaining - l_1
                break
        else:
            print('ERROR: limacon surface length doesnt match list')
            print('limacon_surface = {0}'.format(self.length_total))
            print('surface list = {0}'.format(self.length_list))
            raise(IndexError)
        

        # starting position
        
        p_1 = origin_object.edge[i_pointpos]
        p_2 = origin_object.edge[i_pointpos+1]

        dp = p_2 - p_1
        dp = dp / np.linalg.norm(dp)

        p_start = p_1 + s_remaining * dp


        # starting direction

        theta = random.random() * theta_max
        phi = np.pi * .5 - theta

        if random.random() < 0.5:
            phi *= -1.

        # always points inwards of the limacon
        dp_perp = np.array([-dp[1], dp[0]])

        direction_start = dp_perp @ rotmat(phi)


        return [p_start, direction_start]





    def calc_ray(self, n_bounce_max=1000):
        '''
        bounces a ray with random whisper gallery starting conditions around
        until it leaves the syetem or it was reflected n_bounce_max times  
        '''

        # ray with random starting conditions
        ray = Ray(*self.random_wg_vector())

        for i in range(n_bounce_max):
            ray_still_here = ray.bounce(self)

            if not ray_still_here:
                break
        
        return ray



    def calc_phasespace(self, 
        n_rays=1000, n_bounce_max=1000, 
        outfilename_ps='phasespace.dat', 
        outfilename_ff='phasespace_ff.dat'):
        '''
        calculates the phase space of the array with the given parameters,
        light rays start have whisper gallery starting conditions
        '''

        print('calculating phasespace...')
        print('\tn_rays = {0}'.format(n_rays))
        print('\tn_bouncemax = {0}'.format(n_bounce_max))
        print('\toutfilename_ps = {0}'.format(outfilename_ps))
        print('\toutfilename_ff = {0}'.format(outfilename_ff))
        
        # for phasespace
        s_list = []
        phi_list = []
        # for farfield
        vec_list = []

        for i in range(n_rays):
            print('{0}/{1}'.format(i+1, n_rays))
            ray = self.calc_ray(n_bounce_max=1000)
            
            s_list = s_list + ray.s_list
            phi_list = phi_list + ray.phi_list
            vec_list.append(ray.v)
        
        # finalize phase space
        s_list = np.array(s_list) / self.length_total
        sinphi_list = np.sin( np.array(phi_list) )

        outlist_ps = np.array([s_list, sinphi_list])

        # print(outlist.T)

        np.savetxt(outfilename_ps, outlist_ps)

        # finalize

        ex = np.array([1,0])
        ey = np.array([0,1])

        ff = []

        for v in vec_list:
            dotprod = np.dot(v, ex)
            angle = np.arccos(dotprod)
            
            if v[1] < 0.:
                angle += np.pi
            
            ff.append(angle)
        
        np.savetxt(outfilename_ff, ff)

        return [s_list, sinphi_list, ff]



    ####################################################
    # Plotting
    ####################################################

    def plot_structure(self, ax):
        '''
        plotting structure to ax and resize window to fit the ax
        '''
        
        for g in self.geometry:
            patch = mpatches.Polygon(
                g.edge,
                facecolor='lightgray'
                )
            ax.add_patch(patch)


        ax.set_xlim(*self.bounding_box[0])
        ax.set_ylim(*self.bounding_box[1])

        # saving
        ax.set_aspect('equal')

    

    def plot_structure_aio(self, title='structure.png'):
        '''
        plotting the structure and saving the plot

        aio -> "all in one"
        '''
        print('plotting system...')

        fig, ax = plt.subplots()

        self.plot_structure(ax)

        fig.savefig(title)
        plt.close()
    


    def plot_ray(self, title='ray.png'):
        '''
        plotting an example light ray to see of its working correctly
        '''

        # generate ray

        ray = self.calc_ray(n_bounce_max=1000)
        # print('s_list: ', ray.s_list)
        # print('phi_list: ', ray.phi_list)

        # plot params

        ray_color = 'tab:red'
        arrow_params = dict(
            head_width=0.1,
            length_includes_head=True
            )

        # plotting

        fig, ax = plt.subplots()

        self.plot_structure(ax)

        ax.plot(
            *ray.points[0],
            color=ray_color,
            marker='o'
            )

        ax.plot(
            *np.transpose(ray.points),
            color=ray_color,
            marker=' '
            )
        
        ax.arrow(
            *ray.points[-1],
            *ray.vectors[-1],
            color=ray_color,
            **arrow_params
        )


        fig.savefig(title)
        plt.close()
    


    def plot_phasespace(self, 
        n_rays=1000, n_bounce_max=1000, outfilename='phasespace.png'):
        '''
        calcualtes and plots phasespace
        '''

        s, sinphi, ff = self.calc_phasespace(
            n_rays=n_rays,
            n_bounce_max=n_bounce_max,
            outfilename_ps=outfilename[:-4] + '.dat',
            outfilename_ff=outfilename[:-4] + '_ff.dat'
            )

        

        # plotting
        print('plotting phasespace...')

        tb.plot_phasespace(
            s, 
            sinphi,
            n_refraction=self.geometry[0].n_refraction,
            n_objects=len(self.geometry),
            outfilename=outfilename
            )
        
        tb.plot_phasespace_pixels(
            s, 
            sinphi,
            n_refraction=self.geometry[0].n_refraction,
            n_objects=len(self.geometry),
            outfilename=outfilename[:-4] + '_pixels.png'
            )
        
        print('plotting farfield...')
        tb.plot_farfield(
            ff,
            outfilename=outfilename[:-4] + '_ff.png'
            )
        
        