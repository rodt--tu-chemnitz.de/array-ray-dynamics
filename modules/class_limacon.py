import numpy as np
from shapely.geometry import Polygon
from modules.toolbox import rotmat

class Limacon():
    def __init__(self, 
        R=1, deform=0.4, n_refraction=3.8, n_edgepoints=100, center=np.array([.0,.0]), 
        rot=0.):
        
        # parameters identifying the limacon
        self.R = R
        self.deform = deform
        self.n_edgepoints = n_edgepoints
        self.center = center
        

        # generating polygon
        phi_list = np.linspace(0., 2. * np.pi, n_edgepoints+1)
        r_list = self.edgepoint(phi_list)

        edgepoints = []
        M = rotmat(rot) # rotational matrix

        for phi, r in zip(phi_list, r_list):
            x = r * np.cos(phi)
            y = r * np.sin(phi)
            xy = np.array([x, y]) @ M
            edgepoints.append(xy)
        
        edgepoints = np.array(edgepoints) + center
        self.polygon = Polygon(edgepoints)


        # generate length list
        self.length_total = .0
        self.length_list = [0.]

        for i in range(n_edgepoints):
            p_1 = edgepoints[i]
            p_2 = edgepoints[i+1]

            dist = np.linalg.norm(p_2-p_1)

            self.length_total += dist
            self.length_list.append(self.length_total)

        # critical angle
        self.n_refraction = n_refraction
        self.theta_crit = np.pi * .5 - np.arcsin( 1. / self.n_refraction )
        # print('Limacon critical theta = {0:.6f} pi'.format(self.theta_crit / np.pi))
    


    @property
    def edge(self):
        return np.array(self.polygon.exterior.coords)

    @property
    def bounds(self):
        return self.polygon.bounds

    def edgepoint(self, phi):
        r_now = self.R * (1. + self.deform * np.cos(phi))
        return r_now