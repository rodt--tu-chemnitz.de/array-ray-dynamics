import matplotlib.pyplot as plt

import numpy as np

def rotmat(phi):
    sin = np.sin(phi)
    cos = np.cos(phi)
    
    M = [
        [cos, sin],
        [-sin, cos]
        ]
    
    return np.array(M)


#####################################
# Plotting
#####################################

def plot_phasespace(
    s, sinphi, n_refraction=None, n_objects=1, s_parts=None,
    outfilename='phasespace.png'):
    
        fig, ax = plt.subplots(
            figsize=(4*n_objects,3)
            )

        ax.plot(
            s,
            sinphi,
            marker='o',
            linestyle=' ',
            markersize=1
            )
        
        # mark leaky region
        if n_refraction is not None:
            leaky_region = np.array([-1, 1]) / n_refraction

            ax.axhspan(
                *leaky_region,
                zorder=0,
                color='lightgray',
                # linestyle='--'
                )
        
        # mark geometry objects
        if s_parts is not None:
            vertlines = s_parts
        elif n_objects > 1:
            vertlines = np.linspace(0., 1., n_objects+1)[1:-1]
        else:
            vertlines = []

        
        for x in vertlines:
            ax.axvline(
                x,
                zorder=3,
                color='black',
                linestyle='-'
                )

        # saving
        
        ax.set_xlim(0, 1)
        ax.set_xticks([0, 0.5, 1])
        
        ax.set_ylim(-1, 1)
        if n_refraction is not None:
            ax.set_yticks([-1, -1./n_refraction, 0, 1./n_refraction, 1])
            ax.set_yticklabels([-1, r'$-1/n$', 0, r'$1/n$', 1])
        else:
            ax.set_yticks([-1, 0, 1])

        ax.set_xlabel(r'$s / s_\mathrm{max}$')
        ax.set_ylabel(r'$\sin(\varphi)$')

        fig.savefig(outfilename)
        plt.close()



def plot_phasespace_pixels(
    s, sinphi, n_refraction=None, n_objects=1, s_parts=None,
    res_x_perobj=200, res_y=130,
    outfilename='phasespace.png'):

    fig, ax = plt.subplots(
        figsize=(4*n_objects, 3)
        )

    # prepare data

    H, xedges, yedges = np.histogram2d(
        s, 
        sinphi,
        bins=[res_x_perobj*n_objects+1, res_y+1], # +1 to avoid white lines on borders
        density=True
        )

    ax.imshow(
        H.T,
        interpolation='none',
        cmap='cividis'
        )


    # supplementary lines

    if n_objects > 1:
        vertlist = [x * res_x_perobj for x in range(1,n_objects)]

        for x in vertlist:
            ax.axvline(
                x,
                linestyle='-',
                color='white',
                linewidth=1.
                )

    # axes ticks

    xticks = [x * res_x_perobj for x in range(n_objects+1)]
    ax.set_xticks(xticks)
    xticklabels = [r'${0}$'.format(i, n_objects) for i in range(n_objects+1)]
    ax.set_xticklabels(xticklabels)

    if n_refraction is not None:
        leakyregion = (np.array([-1., 1.]) / n_refraction + 1.) * res_y * .5
        leakyregion_labels =  [r'$- 1 / n$', r'$1 / n$']

        for y in leakyregion:
            ax.axhline(
                y,
                color='white',
                linestyle='--',
                linewidth=1.
            )
    else:
        leakyregion = np.array([])
        leakyregion_labels = []

    yticks = np.array([0, 0.5, 1.]) * res_y
    ax.set_yticks(np.concatenate((yticks,leakyregion))) 
    yticklabels = [-1, 0, 1]
    ax.set_yticklabels(yticklabels + leakyregion_labels)

    # saving

    ax.set_xlabel(r'$s / s_\mathrm{limacon}$')
    ax.set_ylabel(r'$\sin(\varphi)$')

    fig.savefig(outfilename)
    plt.close()



def plot_farfield(ff, outfilename='farfield_ff.png'):
    # cleaning up data

    n_bins = 50

    data, bin_edges = np.histogram(ff, range=(0, 2*np.pi), bins=n_bins)
    bin_centers = [np.mean([bin_edges[i], bin_edges[i+1]]) for i in range(n_bins)]

    data = data / np.max(data)
    
    data = np.concatenate((data, [data[0]]))
    bin_centers = np.concatenate((bin_centers, [bin_centers[0]]))

    # plotting

    fig, ax = plt.subplots(
        subplot_kw={'projection' : 'polar'},
        figsize=(4,4)
        )

    ax.plot(
        bin_centers, 
        data,
        zorder=10
        )

    rticks = np.array([0, .5, 1])
    ax.set_rticks(rticks)
    ax.set_rlim(0,1)

    xticks = np.array([0, .25, .5, .75, 1., 1.25, 1.5, 1.75])*np.pi
    ax.set_xticks(xticks)
    ax.set_xticklabels([r'$3/2 \, \pi$', '', r'$0 \, \pi$', '', r'$1/2 \, \pi$', '', r'$1 \, \pi$', ''])

    ax.grid(
        b=True,
        zorder=0,
        linestyle='--'
        )

    ax.tick_params(axis='x', which='major', pad=10)

    fig.savefig(outfilename)