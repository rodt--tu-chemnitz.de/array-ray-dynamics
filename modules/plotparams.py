import numpy as np


# plot style
import matplotlib

matplotlib.rcParams['savefig.dpi'] = 300
matplotlib.rcParams['savefig.bbox'] = 'tight'

matplotlib.rcParams['xtick.direction'] = 'in'
matplotlib.rcParams['xtick.top'] = True
matplotlib.rcParams['ytick.direction'] = 'in'
matplotlib.rcParams['ytick.right'] = True
matplotlib.rcParams["axes.axisbelow"] = False


matplotlib.rcParams['figure.figsize'] = 4.,3.
#font.sans-serif     : DejaVu Sans, Bitstream Vera Sans, Lucida Grande, Verdana, Geneva, Lucid, Arial, Helvetica, Avant Garde, sans-serif



def setlimits(ax, x_data, y_data, factor=.1):
    '''
    setting the borders of a plot
    '''

    # set xlim

    x_min = np.min(x_data)
    x_max = np.max(x_data)
    x_delta = x_max - x_min
    x_border = x_delta * factor

    ax.set_xlim(
        x_min - x_border,
        x_max + x_border
        )
    

    # set ylim

    y_min = np.min(y_data)
    y_max = np.max(y_data)
    y_delta = y_max - y_min
    y_border = y_delta * factor

    ax.set_ylim(
        y_min - y_border,
        y_max + y_border
        )


def centertitle(s, width=40, size='big'):
    len_s = len(s)

    width_left = int( (width-len_s) * .5)

    if size == 'big':
        centerstring = ' '*width_left + s

        print()
        print('-'*width)
        print(centerstring)
        print('-'*width)
        print()
    
    elif size == 'small':
        centerstring = '-' * width_left + s + '-' * width_left
        if len(centerstring) < width:
            centerstring = centerstring + '-'

        print()
        print(centerstring)
        print()