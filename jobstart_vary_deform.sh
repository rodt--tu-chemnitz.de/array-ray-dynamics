LANG=en_us

# parameters
n_limacons=3
# deform=0.4
DISTperR=3.00
n_rays=20000
n_edgepoints=80

echo "VARY DEFORM"


for deform in $(seq 0.20 0.05 0.90 ); do
    echo $deform

    # create folder
    target_folder="limacons-${n_limacons}/deform-${deform}_dpr-${DISTperR}_edgepoints-${n_edgepoints}_rays-${n_rays}"
    target_folder="data/$target_folder"
    mkdir -p "$target_folder"

    # save input
    input="$n_limacons $deform $DISTperR $n_edgepoints $n_rays $target_folder"

    # execute code
    python main.py $input > "${target_folder}/outfile.txt"
done