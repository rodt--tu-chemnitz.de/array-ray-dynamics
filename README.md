# Array Ray Dynamics

Ray dynamic calculator for arrays of microcavities seperated by air, with emphasis on limaçon-shaped cavities.

Light rays begin their path at random locations on the cavity surfaces with whisper-gallery mode starting conditions, after which the bounce around following Snell's law until they leave the system. The bounce locations and exit angles are used to calculate the phase space and far field of the system.


## How to use

The programm can be run by executing `main.py`. All parameters defining the system are passed to `main.py` in the command line. Thus it is easy to cycle through parameters using simple `bash` scripts.

Running `jobstart_main.sh` executes `main.py` one time with the given set of parameters, which includes e.g. the number of limaçons, the deformation and the distance between their centers. In `jobstart_vary_*.sh` selected parameters are varied in order to analyze their respective impact.

All results are saved to the `data/` folder. They can be compared with the scripts in `compare/`, where the phase spaces and far fields are neatly displayed without the need to rummage through individual results folders.


## Dependencies

To install the libraries necessary to execute the code, just run `conda install $(cat dependencies.txt)` in a bash terminal. All packages are available in the default `conda` channels.


## Further Application

Theoretically, the array needn't consist only of limaçons. Any arbitrary geometry can be achieved by modifying `main.py`, as long the system is represented as polygons which don't overlap and are seperated by air. Each object can have its own refractional index. In that case the leaky area in the phasespace plots needs to be modified, as it depends on the medium of the objects.